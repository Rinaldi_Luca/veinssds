#include "mobility/traci/TraCITLlogic.h"

using Veins::TraCITLlogic;
using Veins::TraCITLphase;
using namespace std;


TraCITLlogic::TraCITLlogic(string id, uint32_t currentPhaseId):
	id(id), currentPhaseId(currentPhaseId) {
}

void TraCITLlogic::addTLphase(uint32_t phaseId, uint32_t duration, string definition) {
    TraCITLphase phase (phaseId, duration, definition);
    tlPhases.push_back(phase);
}
