#ifndef WORLD_TRACI_TRACITLDEFINITION_H
#define WORLD_TRACI_TRACITLDEFINITION_H

#include <omnetpp.h>
#include "mobility/traci/TraCITLlogic.h"
using namespace std;

namespace Veins {

/**
 * TraCI TrafficLight definition
 */
class TraCITLdefinition {
	public:
    TraCITLdefinition(string id);
    void addTLlogic(string id, uint32_t currentPhaseId);
    void addTLphase(string logicId, uint32_t id, uint32_t duration, string definition);
    TraCITLlogic *getTLlogic(string id = "");

	public:
		string id;
		list<string> lanes;

	protected:
		list<TraCITLlogic> tlLogics;
};

}

#endif

