#include "mobility/traci/TraCITLdefinition.h"


using Veins::TraCITLdefinition;
using Veins::TraCITLlogic;
using namespace std;


TraCITLdefinition::TraCITLdefinition(string id):
	id(id) {
}

void TraCITLdefinition::addTLlogic(string id, uint32_t currentPhaseId) {
    TraCITLlogic logic (id, currentPhaseId);
    tlLogics.push_back(logic);
}

void TraCITLdefinition::addTLphase(string logicId, uint32_t id, uint32_t duration, string definition) {
    TraCITLlogic *logic = this->getTLlogic(logicId);
    if(logic) {
        logic->addTLphase(id, duration, definition);
    }
}

TraCITLlogic *TraCITLdefinition::getTLlogic(string id) {
    if(id.empty()) {
        return &(tlLogics.front());
    }
    for (list<TraCITLlogic>::iterator iterator = tlLogics.begin(), end = tlLogics.end(); iterator != end; ++iterator) {
        if(!id.compare(id)) {
            return &(*iterator);
        }
    }
    return NULL;
}
