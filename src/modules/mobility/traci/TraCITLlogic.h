#ifndef WORLD_TRACI_TRACITLLOGIC_H
#define WORLD_TRACI_TRACITLLOGIC_H

#include <omnetpp.h>
#include "mobility/traci/TraCITLphase.h"
using namespace std;

namespace Veins {

/**
 * TraCI TrafficLight logic definition
 */
class TraCITLlogic {
	public:
    TraCITLlogic(string id, uint32_t currentPhaseId);
    void addTLphase(uint32_t phaseId, uint32_t duration, string definition);

	public:
		string id;
		uint32_t currentPhaseId;
		vector<TraCITLphase> tlPhases;
};

}

#endif

