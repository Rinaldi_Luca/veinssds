#ifndef WORLD_TRACI_TRACITLPHASE_H
#define WORLD_TRACI_TRACITLPHASE_H

#include <omnetpp.h>
using namespace std;

namespace Veins {

/**
 * TraCI TrafficLight logic phase definition
 */
class TraCITLphase {
	public:
    TraCITLphase(uint32_t id, uint32_t duration, string definition);

	public:
        uint32_t id;
        uint32_t duration;
        string definition;
};

}

#endif

