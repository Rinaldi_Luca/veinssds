#include "mobility/traci/TraCITLphase.h"

using Veins::TraCITLphase;
using namespace std;


TraCITLphase::TraCITLphase(uint32_t id, uint32_t duration, string definition):
	id(id), duration(duration), definition(definition)  {
}
