//
// Copyright (C) 2006-2011 Christoph Sommer <christoph.sommer@uibk.ac.at>
//
// Documentation for these modules is at http://veins.car2x.org/
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#include "application/traficLightsAlgorithms/GenericAlgorithm.h"
#include <stdio.h>


using Veins::TraCIMobilityAccess;
using Veins::AnnotationManagerAccess;
using namespace std;

const simsignalwrap_t GenericAlgorithm::varTimeStepChangedSignal = simsignalwrap_t(TRACI_SIGNAL_VAR_TIME_STEP);

Define_Module(GenericAlgorithm);

void GenericAlgorithm::initialize(int stage) {
	BaseWaveApplLayer::initialize(stage);
	EV << stage << " stage\n";

	/*if(stage == 0) {
	    findHost()->subscribe(varTimeStepChangedSignal, this);
	    EV << simTime() << "!!!!!!!!!!!!!!!!!!!!!!!!!!\n";
	    scheduleAt(simTime()+1, new cMessage("PIPPO"));
	}*/

	if (stage == 1) {
		traci = TraCIMobilityAccess().get(getParentModule());
		//scheduleAt(simTime()+3, new cMessage("PIPPO"));
		//int32_t tfLightCount = traci->getCommandInterface()->getTLCount();
		// Prende la lista degli id dei semafori
		/*list<string> tlIds = traci->getCommandInterface()->getTLIdList();
		// Itera sui semafori
		for (list<string>::const_iterator iterator = tlIds.begin(), end = tlIds.end(); iterator != end; ++iterator) {
		    string state = traci->getCommandInterface()->getTLState(iterator->c_str());
		    EV << iterator->c_str() << " - " << state << "\n";
		}*/
		//traci->getCommandInterface()->setTrafficLightProgram("60878103", "0");
		//traci->getCommandInterface()->setTrafficLightPhaseIndex("60878103", 3);
		//traci->getCommandInterface()->setTrafficLightProgram("10", "myProgramRed");

		/*annotations = AnnotationManagerAccess().getIfExists();
		ASSERT(annotations);

		sentMessage = false;
		lastDroveAt = simTime();
		findHost()->subscribe(parkingStateChangedSignal, this);
		isParking = false;
		sendWhileParking = par("sendWhileParking").boolValue();*/
	}
}

void GenericAlgorithm::finish() {
}

void GenericAlgorithm::handleMessage(cMessage *msg) {
    if (msg->isSelfMessage()) {
        /*double speed = traci->getSpeed();
        EV << speed << "!!!!!!!!!!!!!!!!!!!!!!!!!!\n";

        scheduleAt(simTime()+3, new cMessage("PIPPO"));
        return;*/
    }
    error("GenericAlgorithm doesn't handle messages from other modules");
}

void GenericAlgorithm::onBeacon(WaveShortMessage* wsm) {
}

void GenericAlgorithm::onData(WaveShortMessage* wsm) {
	/*findHost()->getDisplayString().updateWith("r=16,green");
	annotations->scheduleErase(1, annotations->drawLine(wsm->getSenderPos(), traci->getPositionAt(simTime()), "blue"));

	if (traci->getRoadId()[0] != ':') traci->commandChangeRoute(wsm->getWsmData(), 9999);
	if (!sentMessage) sendMessage(wsm->getWsmData());*/
}

void GenericAlgorithm::sendMessage(std::string blockedRoadId) {
	/*sentMessage = true;

	t_channel channel = dataOnSch ? type_SCH : type_CCH;
	WaveShortMessage* wsm = prepareWSM("data", dataLengthBits, channel, dataPriority, -1,2);
	wsm->setWsmData(blockedRoadId.c_str());
	sendWSM(wsm);*/
}
void GenericAlgorithm::receiveSignal(cComponent* source, simsignal_t signalID, cObject* obj) {
    /*EV << signalID << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n";
	Enter_Method_Silent();
	if (signalID == varTimeStepChangedSignal) {
	    EV << signalID << " STEPPPP !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n";
	}*/
}
void GenericAlgorithm::handleParkingUpdate(cObject* obj) {
	/*isParking = traci->getParkingState();
	if (sendWhileParking == false) {
		if (isParking == true) {
			(FindModule<BaseConnectionManager*>::findGlobalModule())->unregisterNic(this->getParentModule()->getSubmodule("nic"));
		}
		else {
			Coord pos = traci->getCurrentPosition();
			(FindModule<BaseConnectionManager*>::findGlobalModule())->registerNic(this->getParentModule()->getSubmodule("nic"), (ChannelAccess*) this->getParentModule()->getSubmodule("nic")->getSubmodule("phy80211p"), &pos);
		}
	}*/
}
void GenericAlgorithm::handlePositionUpdate(cObject* obj) {
	/*BaseWaveApplLayer::handlePositionUpdate(obj);

	// stopped for for at least 10s?
	if (traci->getSpeed() < 1) {
		if (simTime() - lastDroveAt >= 10) {
			findHost()->getDisplayString().updateWith("r=16,red");
			if (!sentMessage) sendMessage(traci->getRoadId());
		}
	}
	else {
		lastDroveAt = simTime();
	}*/
}
void GenericAlgorithm::sendWSM(WaveShortMessage* wsm) {
	/*if (isParking && !sendWhileParking) return;
	sendDelayedDown(wsm,individualOffset);*/
}
