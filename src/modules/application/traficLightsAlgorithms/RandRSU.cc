//
// Copyright (C) 2006-2011 Christoph Sommer <christoph.sommer@uibk.ac.at>
//
// Documentation for these modules is at http://veins.car2x.org/
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#include "application/traficLightsAlgorithms/RandRSU.h"
#include "modules/mobility/traci/TraCIScenarioManager.h"
#include "modules/mobility/traci/TraCICommandInterface.h"
#include "modules/mobility/traci/TraCITLdefinition.h"
#include "modules/mobility/traci/TraCITLlogic.h"
#include "modules/mobility/traci/TraCITLphase.h"

using Veins::AnnotationManagerAccess;
using Veins::TraCIMobilityAccess;
using Veins::TraCIScenarioManagerAccess;
using Veins::TraCIScenarioManager;
using Veins::TraCITLdefinition;
using Veins::TraCITLlogic;
using Veins::TraCITLphase;
using namespace std;

Define_Module(RandRSU);

void RandRSU::initialize(int stage) {
	BaseWaveApplLayer::initialize(stage);

	timeoutMessage = new cMessage(TIMEOUT_MESSAGE);

	if (stage == 0) {
	    mobi = dynamic_cast<BaseMobility*> (getParentModule()->getSubmodule("mobility"));
	    ASSERT(mobi);
	    annotations = AnnotationManagerAccess().getIfExists();
	    ASSERT(annotations);
	    this->setTimeout(1);
	}

}

void RandRSU::onBeacon(WaveShortMessage* wsm) {

}

void RandRSU::setTimeout(int seconds) {
    scheduleAt(simTime()+seconds, timeoutMessage);
}

void RandRSU::saveTlDefinitions() {
    if (tlDefinitions.empty()) {
        TraCIScenarioManager* tra = TraCIScenarioManagerAccess().get();
        if (tra && tra->isConnected()) {
            list<string> tlIds = tra->getCommandInterface()->getTLIdList();
            for (list<string>::const_iterator iterator = tlIds.begin(), end =
                    tlIds.end(); iterator != end; ++iterator) {
                TraCITLdefinition result =
                                    tra->getCommandInterface()->getTLCompleteDefinition(
                                            iterator->c_str());
                tlDefinitions.push_back(result);
            }
            changeDuration = true;
        }
    }
}

void RandRSU::setTLPhaseDuration() {
    TraCIScenarioManager* tra = TraCIScenarioManagerAccess().get();
    if (tra && tra->isConnected()) {
        for (list<TraCITLdefinition>::iterator iterator = tlDefinitions.begin(), end = tlDefinitions.end(); iterator != end; ++iterator) {
            uint32_t phase = tra->getCommandInterface()->getTLCurrentPhase(iterator->id);
            string program = tra->getCommandInterface()->getTLCurrentProgram(iterator->id);
            TraCITLlogic *logic = iterator->getTLlogic(program);
            if(changeDuration || logic->currentPhaseId != phase) {
                string phaseDef = logic->tlPhases[phase].definition;
                int phaseOriginalDuration = logic->tlPhases[phase].duration;
                if (phaseDef.find("y") == string::npos) {
                    int duration = rand() % 30000 + 2000;
                    tra->getCommandInterface()->setTrafficLightCurrentPhaseDuration(iterator->id, duration);

                    //EV << ">>>>>>>>>>" << phaseOriginalDuration << " - - " << phaseDef << " - -" << duration << "<<<<<<<<<<<<<<<\n";
                }
                logic->currentPhaseId = phase;
            }
        }
        int lanes = tra->getCommandInterface()->getLanesCount();
        int edges = tra->getCommandInterface()->getEdgesCount();
        //double time = tra->getCommandInterface()->getWaitingTime("1");
        //EV << ">>>>>>>>>> TIME: " << time << "<<<<<<<<<<<<<<<\n";

        changeDuration = false;

        /*TraCITLdefinition result = tra->getCommandInterface()->getTLCompleteDefinition("60878103");
        TraCITLlogic *logic = result.getTLlogic();
        EV << ">>>>>>>>>>" << result.id << " - " << logic->id << " - " << logic->currentPhaseId << " - "  <<logic->tlPhases.size() << "<<<<<<<<<<<<<<<<<<\n";
        for (list<TraCITLphase>::iterator iterator = logic->tlPhases.begin(), end = logic->tlPhases.end(); iterator != end; ++iterator) {
            EV << ">>>>>>>>>>" << "Phase: " << iterator->id <<" duration: " << iterator->duration << " definition: " << iterator->definition << "<<<<<<<<<<<<<<<\n";
        }*/

        //list<string> tlIds = tra->getCommandInterface()->getLanesList("60878103");
        /*for (list<string>::const_iterator iterator = tlIds.begin(), end = tlIds.end(); iterator != end; ++iterator) {
            int carsN = tra->getCommandInterface()->getLaneCarsNumber(iterator->c_str());
            //EV << iterator->c_str() << "-" <<  carsN << "\n";
        }*/
    }
}

void RandRSU::handleMessage(cMessage *msg) {
    if (msg->isSelfMessage()) {
        if(!strcmp(msg->getName(), TIMEOUT_MESSAGE)) {
            this->saveTlDefinitions();
            this->setTLPhaseDuration();
            this->setTimeout(2);
        }
        return;
    }
    error("RandRSU doesn't handle messages from other modules");
}

void RandRSU::onData(WaveShortMessage* wsm) {
	findHost()->getDisplayString().updateWith("r=16,green");

	annotations->scheduleErase(1, annotations->drawLine(wsm->getSenderPos(), mobi->getCurrentPosition(), "blue"));

	if (!sentMessage) sendMessage(wsm->getWsmData());
}

void RandRSU::sendMessage(std::string blockedRoadId) {
	sentMessage = true;
	t_channel channel = dataOnSch ? type_SCH : type_CCH;
	WaveShortMessage* wsm = prepareWSM("data", dataLengthBits, channel, dataPriority, -1,2);
	wsm->setWsmData(blockedRoadId.c_str());
	sendWSM(wsm);
}
void RandRSU::sendWSM(WaveShortMessage* wsm) {
	sendDelayedDown(wsm,individualOffset);
}
