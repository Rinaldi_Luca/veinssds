//
// Copyright (C) 2006-2011 Christoph Sommer <christoph.sommer@uibk.ac.at>
//
// Documentation for these modules is at http://veins.car2x.org/
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//

#include "application/traficLightsAlgorithms/TimerRSU.h"
#include "modules/mobility/traci/TraCIScenarioManager.h"
#include "modules/mobility/traci/TraCICommandInterface.h"
#include "modules/mobility/traci/TraCITLdefinition.h"
#include "modules/mobility/traci/TraCITLlogic.h"
#include "modules/mobility/traci/TraCITLphase.h"

using Veins::AnnotationManagerAccess;
using Veins::TraCIMobilityAccess;
using Veins::TraCIScenarioManagerAccess;
using Veins::TraCIScenarioManager;
using Veins::TraCITLdefinition;
using Veins::TraCITLlogic;
using Veins::TraCITLphase;
using namespace std;

Define_Module(TimerRSU);

void TimerRSU::initialize(int stage) {
	BaseWaveApplLayer::initialize(stage);

	timeoutMessage = new cMessage(TIMEOUT_MESSAGE);

	if (stage == 0) {
	    mobi = dynamic_cast<BaseMobility*> (getParentModule()->getSubmodule("mobility"));
	    ASSERT(mobi);
	    annotations = AnnotationManagerAccess().getIfExists();
	    ASSERT(annotations);
	    timer = TIMER_INITIAL_VALUE;
	    this->setTimeout(1);
	}

}

void TimerRSU::onBeacon(WaveShortMessage* wsm) {

}

void TimerRSU::setTimeout(int seconds) {
    scheduleAt(simTime()+seconds, timeoutMessage);
}

// <!> Error in module (Veins::TraCIScenarioManagerLaunchd) RSUExampleScenario.manager
// (id=6) at event #796, t=662: Model error: ASSERT: condition count == drivingVehicleCount false
// in function processVehicleSubscription, modules/mobility/traci/TraCIScenarioManager.cc line 705.

void TimerRSU::saveTlDefinitions() {
    if (tlDefinitions.empty()) {
        TraCIScenarioManager* tra = TraCIScenarioManagerAccess().get();
        if (tra && tra->isConnected()) {
            list<string> tlIds = tra->getCommandInterface()->getTLIdList();
            for (list<string>::const_iterator iterator = tlIds.begin(), end =
                    tlIds.end(); iterator != end; ++iterator) {
                TraCITLdefinition result =
                                    tra->getCommandInterface()->getTLCompleteDefinition(
                                            iterator->c_str());
                list<string> tlIds = tra->getCommandInterface()->getLanesList(iterator->c_str());
                result.lanes = tlIds;

                TraCITLlogic *logic = result.getTLlogic();

                for(vector<TraCITLphase>::iterator piterator = logic->tlPhases.begin(),
                        end = logic->tlPhases.end(); piterator != end; ++piterator) {
                    string phaseDef = piterator->definition;
                    int phaseOriginalDuration = piterator->duration;
                    if (phaseDef.find("y") == string::npos) {
                        int duration = phaseOriginalDuration*3;
                        tra->getCommandInterface()->setTrafficLightCurrentPhaseDuration(result.id, duration);
                    }
                }

                int randPhase = 0 + (rand() % (int)(logic->tlPhases.size()-1 - 0 + 1));
                EV << ">>>>>>>>>> Semaforo phase: " << randPhase << "<<<<<<<<<<<<<<<\n";
                // Setting random phase
                tra->getCommandInterface()->setTrafficLightPhaseIndex(result.id, randPhase);
                tlDefinitions.push_back(result);
            }
            changeDuration = true;
        }
    }
}

void TimerRSU::setTLPhaseDuration() {
    TraCIScenarioManager* tra = TraCIScenarioManagerAccess().get();
    if (tra && tra->isConnected()) {
        for (list<TraCITLdefinition>::iterator iterator = tlDefinitions.begin(), end = tlDefinitions.end(); iterator != end; ++iterator) {
            uint32_t phase = tra->getCommandInterface()->getTLCurrentPhase(iterator->id);
            string program = tra->getCommandInterface()->getTLCurrentProgram(iterator->id);
            TraCITLlogic *logic = iterator->getTLlogic(program);
            string phaseDef = logic->tlPhases[phase].definition;
            int counterStopped = 0;

            EV << ">>>>>>>>>> Semaforo: " << iterator->id << "<<<<<<<<<<<<<<<\n";
            for (list<string>::iterator literator = iterator->lanes.begin(), end = iterator->lanes.end(); literator != end; ++literator) {
                int carsN = tra->getCommandInterface()->getLaneCarsNumber(literator->c_str());
                int index = distance(iterator->lanes.begin(), literator);

                if(phaseDef[index] == 'r' || phaseDef[index] == 'R')
                    counterStopped+=carsN;
            }
            if (counterStopped >= timer) {
                if (timer == 0)
                    timer = TIMER_INITIAL_VALUE;
                else
                    timer = counterStopped;
                int nextPhase = ((phase+1) % logic->tlPhases.size());
                EV << "Stopped - " <<  counterStopped << " - nextPhase " << nextPhase << "\n";
                tra->getCommandInterface()->setTrafficLightPhaseIndex(iterator->id, nextPhase);
            }
            else
                timer--;

        }
        changeDuration = false;
    }
}

void TimerRSU::handleMessage(cMessage *msg) {
    if (msg->isSelfMessage()) {
        if(!strcmp(msg->getName(), TIMEOUT_MESSAGE)) {
            this->saveTlDefinitions();
            this->setTLPhaseDuration();
            this->setTimeout(5);
        }
        return;
    }
    error("RandRSU doesn't handle messages from other modules");
}

void TimerRSU::onData(WaveShortMessage* wsm) {
	findHost()->getDisplayString().updateWith("r=16,green");

	annotations->scheduleErase(1, annotations->drawLine(wsm->getSenderPos(), mobi->getCurrentPosition(), "blue"));

	if (!sentMessage) sendMessage(wsm->getWsmData());
}

void TimerRSU::sendMessage(std::string blockedRoadId) {
	sentMessage = true;
	t_channel channel = dataOnSch ? type_SCH : type_CCH;
	WaveShortMessage* wsm = prepareWSM("data", dataLengthBits, channel, dataPriority, -1,2);
	wsm->setWsmData(blockedRoadId.c_str());
	sendWSM(wsm);
}
void TimerRSU::sendWSM(WaveShortMessage* wsm) {
	sendDelayedDown(wsm,individualOffset);
}
