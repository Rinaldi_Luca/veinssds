import sys
import csv

def readCsvFile(path):
    res = {}

    with open(path, 'r') as csvfile:
        data = csv.reader(csvfile)
        headers = next(data, None)
        counter = 0

        for index, name in enumerate(headers):
            if name != "X":
                module = name.split("/")[1]
                car = name.split(".")[1]
                res[car] = res[car] if car in res else {}
                res[car][module] = res[car][module] if module in res[car] else {}
                res[car]["ids"] = res[car]["ids"] if "ids" in res[car] else {}
                res[car]["ids"][module] = index

        for row in data:
            for car in res.values():
                if "co2emission" in car["ids"]:
                    index = car["ids"]["co2emission"]
                    if len(row[index]):
                        car["co2emission"][float(row[0])] = float(row[index])
                if "distance" in car["ids"]:
                    index = car["ids"]["distance"]
                    if len(row[index]):
                        car["distance"][float(row[0])] = float(row[index])
            counter+=1
            print(counter)

    return res

def recordData(data):
    dataTotal = {}
    totalCars = {}
    for car in data.values():
        if "co2emission" in car:
            co2emissionTotal = {}
            for key, value in car["co2emission"].items():
                co2emissionTotal[key] = co2emissionTotal[key-1] if key-1 in co2emissionTotal else 0
                co2emissionTotal[key]+= value
                co2emissionTotal[key] = round(co2emissionTotal[key],2)

            car["co2tot"] = co2emissionTotal
            if "distance" in car:
                car["rapporto"] = {}
                for key, value in car["co2tot"].items():
                    totalCars[key] = totalCars[key] if key in totalCars else 0
                    totalCars[key]+= 1
                    if car["distance"][key]:
                        car["rapporto"][key] = round(value/car["distance"][key],4)
                        dataTotal[key] = dataTotal[key] if key in dataTotal else 0
                        dataTotal[key]+= car["rapporto"][key]
                        dataTotal[key] = round(dataTotal[key],2)

    for key, value in dataTotal.items():
        dataTotal[key] = round(value/totalCars[key], 2)

    return dataTotal;


if len(sys.argv) < 3:
    print("Usage: *.py folder outputFile")
    exit()

data = recordData(readCsvFile(sys.argv[1]));

with open(sys.argv[2], 'w', newline='') as csvfile:
    csvwriter = csv.writer(csvfile)
    csvwriter.writerow(['time', 'value'])
    for key, value in data.items():
        csvwriter.writerow([key, value])

print("Writted %d items to %s" % (len(data.keys()), sys.argv[2]))