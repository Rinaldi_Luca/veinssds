import sys
import glob
import csv

def readCsvFile(path):
    res = {}
    with open(path, 'r') as csvfile:
        data = csv.reader(csvfile)
        headers = next(data, None)

        module = headers[1].split(".")
        res["type"] = module[3]
        res["car"] = module[1]
        res["values"] = {}
        for row in data:
            res["values"][float(row[0])] = float(row[1])

    return res

def recordData(filenames):
    res = {}
    dataTotal = {}
    for name in filenames:
        data = readCsvFile(name)
        res[data["car"]] = res[data["car"]] if data["car"] in res else {}
        res[data["car"]][data["type"]] = data["values"]

    for car in res.values():
        if "co2emission" in car:
            co2emissionTotal = {}
            for key, value in car["co2emission"].items():
                co2emissionTotal[key] = co2emissionTotal[key-1] if key-1 in co2emissionTotal else 0
                co2emissionTotal[key]+= value
                co2emissionTotal[key] = round(co2emissionTotal[key],2)

            car["co2tot"] = co2emissionTotal
            if "distance" in car:
                car["rapporto"] = {}
                for key, value in car["co2tot"].items():
                    car["rapporto"][key] = round(value/car["distance"][key],4)
                    dataTotal[key] = dataTotal[key] if key in dataTotal else 0
                    dataTotal[key]+= car["rapporto"][key]
                    dataTotal[key] = round(dataTotal[key],2)
    print(res['node[0]'])
    return dataTotal;


if len(sys.argv) < 3:
    print("Usage: *.py folder outputFile")
    exit()

filenames = glob.glob(sys.argv[1] + "/*.csv");


data = recordData(filenames);
exit()

with open(sys.argv[2], 'w', newline='') as csvfile:
    csvwriter = csv.writer(csvfile)
    csvwriter.writerow(['time', 'value'])
    for key, value in data.items():
        csvwriter.writerow([key, value])

print("Writted %d items to %s" % (len(data.keys()), sys.argv[2]))