import sys
import csv
import matplotlib.pyplot as plt
from itertools import cycle
import glob

if len(sys.argv) < 2:
    print("Usage: *.py folder outputFile")
    exit()

def readCsvFile(path):
    res = {}

    with open(path, 'r') as csvfile:
        data = csv.reader(csvfile)
        headers = next(data, None)
        for row in data:
            car = row[2].split(".")[1]
            res[car] = res[car] if car in res else {}
            res[car][row[3]] = row[4]
        
    return res

def getData(groupdata, prop):
    glen = len(groupdata)
    if glen == 1:
        return (groupdata[0],None,None)

    meanData = {}
    minData = {}
    maxData = {}

    for data in groupdata:
        for name, car in data.items():
            meanData[name] = meanData[name] if name in meanData else {}
            meanData[name][prop] = meanData[name][prop] if prop in meanData[name] else 0
            meanData[name][prop] += float(car[prop])
            """
            minData[name] = minData[name] if name in minData else val
            minData[name] = min(minData[name], val)
            maxData[name] = maxData[name] if name in maxData else val
            maxData[name] = max(maxData[name], val)
            """

    for name, car in meanData.items():
        meanData[name][prop] = round(car[prop]/glen, 2)

    return (meanData, minData, maxData)

colors = cycle('bgrcmykw')
def drawGroup(name, groupdata, prop):
    data, minData, maxData = getData(groupdata, prop)
    values = list(map(lambda x: float(x[prop]), data.values()))
    print(name, sum(values))
    plt.hist(list(values), color=next(colors), histtype='bar', label=name)


groups = {}
order = []
for pattern in sys.argv[1:]:
    for filename in glob.glob(pattern):
        groupname = filename[filename.rfind('/')+1:filename.rfind('.')]
        groupname = groupname[0: groupname.rfind('_') if groupname.rfind('_') != -1 else len(groupname)]
        groups[groupname] = groups[groupname] if groupname in groups else []
        groups[groupname].append(readCsvFile(filename))
        if not groupname in order:
            order.append(groupname)

rows = round(len(groups.keys())/2)
index = 1

prop = "totalTime"
dataToDraw = []
labels = []
for name in order:
    data, minData, maxData = getData(groups[name], prop)
    values = list(map(lambda x: float(x[prop]), data.values()))
    print(name, sum(values))
    dataToDraw.append(list(values))
    labels.append(name)

plt.hist(dataToDraw, color=['y','b','g','c','m', 'r', 'k'][0:len(labels)], histtype='bar', label=labels)
plt.xlabel('Tempo (sec)')
plt.ylabel('Numero auto')
legend = plt.legend(loc='upper right', shadow=True, fontsize=10)
plt.grid(True)
plt.show();