import sys
import csv
import matplotlib.pyplot as plt
from itertools import cycle
import glob

if len(sys.argv) < 2:
    print("Usage: *.py csvfiles")
    exit()


def readCsvFile(path):
    res = {}
    with open(path, 'r') as csvfile:
        data = csv.reader(csvfile)
        headers = next(data, None)
        for row in data:
            res[float(row[0])] = float(row[1])

    return res

def getData(groupdata):
    glen = len(groupdata)
    if glen == 1:
        return (groupdata[0],None,None)

    meanData = {}
    minData = {}
    maxData = {}

    for data in groupdata:
        for time, val in data.items():
            meanData[time] = meanData[time] if time in meanData else 0
            meanData[time]+=val
            minData[time] = minData[time] if time in minData else val
            minData[time] = min(minData[time], val)
            maxData[time] = maxData[time] if time in maxData else val
            maxData[time] = max(maxData[time], val)

    for time, val in meanData.items():
        meanData[time] = round(val/glen, 2)

    return (meanData, minData, maxData)


def drawGroup(name, groupdata, rows, index):
    plt.subplot(rows,2,index)
    colors = cycle('gbmrcyk')
    data, minData, maxData = getData(groupdata)
    media = sum(list(data.values())) / len(list(data.values()))
    massimo = max(list(data.values()));
    if minData and maxData:
        #marker='o', ls='',
        plt.plot(list(minData.keys()),list(minData.values()), color='k', markersize=2, alpha=.2, lw=1, label=name+' minimi')
        plt.plot(list(maxData.keys()),list(maxData.values()), color='r', markersize=2, alpha=.2, lw=1, label=name+' massimi')

    plt.plot(list(data.keys()),list(data.values()), color=next(colors))
    plt.plot([0, 3600], [massimo, massimo],  color=next(colors), lw=3, label='Massimo %.2f' % massimo )
    plt.plot([0, 3600], [media, media],  color=next(colors), lw=3, label='Media %.2f' % media )
    legend = plt.legend(loc='upper left', shadow=True, fontsize=10)
    plt.title(name)
    plt.xlabel('Tempo (sec)')
    plt.ylabel('Media emissioni (CO2/m)')
    plt.axis([0.0,3600.0, 0.0,2.0])
    ax = plt.gca()
    ax.set_autoscale_on(False)
    plt.grid(True)

plt.figure(1)

groups = {}
order = []
for pattern in sys.argv[1:]:
    for filename in glob.glob(pattern):
        groupname = filename[filename.rfind('/')+1:filename.rfind('.')]
        groupname = groupname[0: groupname.rfind('_') if groupname.rfind('_') != -1 else len(groupname)]
        groups[groupname] = groups[groupname] if groupname in groups else []
        groups[groupname].append(readCsvFile(filename))
        if not groupname in order:
            order.append(groupname)

rows = round(len(groups.keys())/2)
index = 1
for name in order:
    drawGroup(name, groups[name], rows, index)
    index+=1

#plt.suptitle("Auto in attesa - poco traffico", fontsize=20)
plt.show();