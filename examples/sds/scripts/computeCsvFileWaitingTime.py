import sys
import csv

def readCsvFile(path):
    res = {}

    with open(path, 'r') as csvfile:
        data = csv.reader(csvfile)
        headers = next(data, None)
        counter = 0

        for index, name in enumerate(headers):
            if name != "X":
                module = name.split("/")[1]
                car = name.split(".")[1]
                res[car] = res[car] if car in res else {}
                res[car][module] = res[car][module] if module in res[car] else {}
                res[car]["ids"] = res[car]["ids"] if "ids" in res[car] else {}
                res[car]["ids"][module] = index

        for row in data:
            for car in res.values():
                index = car["ids"]["speed"]
                if len(row[index]):
                    car["speed"][float(row[0])] = float(row[index])
            counter+=1
            print(counter)

    return res

def recordData(data):
    dataTotal = {}
    totalCars = {}
    totalWaiting = {}
    for car in data.values():
        if "speed" in car:
            """
            waitingTime = {}
            for key, value in car["speed"].items():
                waitingTime[key] = waitingTime[key-1] if key-1 in waitingTime else 0
                waitingTime[key]+= 1 if value < 0.5 else 0
                dataTotal[key] = dataTotal[key] if key in dataTotal else 0
                dataTotal[key]+= waitingTime[key]

            car["waitingTime"] = waitingTime
            """
            for key, value in car["speed"].items():
                totalCars[key] = totalCars[key] if key in totalCars else 0
                totalCars[key]+= 1
                totalWaiting[key] = totalWaiting[key] if key in totalWaiting else 0
                totalWaiting[key]+= 1 if value < 0.5 else 0

    for key, value in totalCars.items():
        dataTotal[key] = (round(totalWaiting[key]/value*100, 2), value)

    return dataTotal;


if len(sys.argv) < 3:
    print("Usage: *.py folder outputFile")
    exit()

data = recordData(readCsvFile(sys.argv[1]));

with open(sys.argv[2], 'w', newline='') as csvfile:
    csvwriter = csv.writer(csvfile)
    csvwriter.writerow(['time', 'value', 'total'])
    for key, value in data.items():
        csvwriter.writerow([key, value[0], value[1]])

print("Writted %d items to %s" % (len(data.keys()), sys.argv[2]))