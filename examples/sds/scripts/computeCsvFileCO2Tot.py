import sys
import csv

def searchValues(rows, index):
    res = {}
    for row in rows:
        if len(row[index]):
            res[float(row[0])] = float(row[index])

    return res


def readCsvFile(path):
    res = {}
    rows = []

    with open(path, 'r') as csvfile:
        data = csv.reader(csvfile)
        headers = next(data, None)
        for row in data:
            rows.append(row)

        for name in headers:
            index = headers.index(name) #change this!!
            if name != "X":
                module = name.split("/")[1]
                car = name.split(".")[1]
                res[car] = res[car] if car in res else {}
                res[car][module] = searchValues(rows, index)
        
    return res

def recordData(data):
    dataTotal = {}

    for car in data.values():
        if "co2emission" in car:
            co2emissionTotal = {}
            for key, value in car["co2emission"].items():
                co2emissionTotal[key] = co2emissionTotal[key-1] if key-1 in co2emissionTotal else 0
                co2emissionTotal[key]+= value
                co2emissionTotal[key] = round(co2emissionTotal[key],2)

            car["co2tot"] = co2emissionTotal
            if "distance" in car:
                car["rapporto"] = {}
                for key, value in car["co2emission"].items():
                    dataTotal[key] = dataTotal[key] if key in dataTotal else 0
                    dataTotal[key]+= value
                    dataTotal[key] = round(dataTotal[key],2)

    return dataTotal;


if len(sys.argv) < 3:
    print("Usage: *.py folder outputFile")
    exit()

data = recordData(readCsvFile(sys.argv[1]));

with open(sys.argv[2], 'w', newline='') as csvfile:
    csvwriter = csv.writer(csvfile)
    csvwriter.writerow(['time', 'value'])
    for key, value in data.items():
        csvwriter.writerow([key, value])

print("Writted %d items to %s" % (len(data.keys()), sys.argv[2]))